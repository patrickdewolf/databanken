use ModernWays
drop table if exists ´Postcodes´;
-- eerst droppen vooraleer aan te maken
CREATE TABLE Postcodes(
    Code CHAR(4),
    Plaats NVARCHAR(50),
    Localite NVARCHAR(50),
    Provincie NVARCHAR(50),
    Provence NVARCHAR(50)
    -- alleen het jaartal, geen datetime
    -- omdat de kleinste datum daarin 1753 is
    -- varchar omdat we ook jaartallen kleiner dan 1000 hebben
   
);